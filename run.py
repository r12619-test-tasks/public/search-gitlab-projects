from flask import Flask, request, render_template
import json
import api
import db


app = Flask(__name__)


def isnull(expression, replacement_value):
    if expression is None:
        return replacement_value
    return expression


def pretty_json(data):
    """ Возвращает json текст с отступами """
    return json.dumps(data, indent=2)


def merge_projects(data1, data2):
    """ Возвращает слияние двух списков проектов без повторов """
    projects = data1.copy()
    ids = [[v for k, v in project.items() if k == 'id'] for project in data1]
    for p in data2:
        project = {k: v for k, v in p.items() if [v] not in ids}
        if project.setdefault('id'):
            projects.append(project)
    return projects


def get_projects(substring):
    """ Возвращает список проектов по ключевому слову
        ищет сначала в базе, потом на сервере gitlab """
    test_db = db.TestDB('test.db')
    data1 = test_db.get_projects(substring)
    data2 = []
    if substring not in test_db.get_substrings():
        data2 = api.projects_search(substring)
    data = merge_projects(data1, data2)
    test_db.add_substring(substring)
    test_db.add_projects(data)
    return data


@app.route('/')
def index():
    """ Web страница поиска проектов по умолчанию localhost http://127.0.0.1:5000/ """
    substring = isnull(request.args.get('search'), '')
    data = []
    if substring:
        data = get_projects(substring)
    return render_template('index.html', table_data=data, pretty_json=pretty_json, json_height=9)


if __name__ == '__main__':
    app.run(debug=True)
